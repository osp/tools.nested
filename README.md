**Nested** or **Nestcloud** is the (provisory) name of a system to manage and serve websites made by OSP. 

It is constructed around:

* a **Nextcloud instance** as the shared-content manager
* a **Gitlab instance** as the website code manager
* **Pelican** as the website generator
* a **VPS** as the server

It focuses on managing a website in collaborative manner, in a way that is both accessible and as close as it can to raw files.

It uses tree-shaped files system as architecture.

It set a bridge between customized edits in your local environment, through your own software, and online shared content.

It starts a permeability between internal collective organisation (private document, meeting notes, etc) and public sharing (article on the website, official release, etc).

It looks like this

```mermaid
flowchart LR

    nextcloud[("nextcloud (content)")]
    git[("git (code)")]
    server[(server)]
    user[(user)]

    I1([cloud.website.com])
    I2([website.com])
    I3([deploy.website.com])
    I4([taste.website.com])
    
    nextcloud <--sync--> user

    nextcloud --sync--> server
    I3 --activate--> server
    git --pull--> server
    

    I1 --access--> nextcloud


    server --public--> I2
    server --preview--> I4

    classDef default stroke:#ccc, stroke-width: 1;
    classDef user stroke:hotpink, stroke-width: 4;
    classDef service stroke:SpringGreen, stroke-width: 4;
    classDef domain stroke:DeepSkyBlue, stroke-width: 4;

    class nextcloud,git,server service
    class user user
    class I1,I2,I3,I4 domain
```

`sync` is done by using the Nextcloud sync client for automatic synchronisation while a file is edited.

`push` and `pull` is using git to update the website.

`deploy.website.com` is a webpage coded with NodeJS that launches script on the VPS at the click of HTML buttons. It is used to regenerate the website or pull modifications. It can regenarate `website.com` or `taste.website.com` which is a tasting version before baking and serving to the public.

All the content of the website is on a specific `www` folder of your Nextcloud instance. The content present itself as a collection of files nested inside subfolders, usually markdown `.md` file, alongside other media files (`jpg`, `mp3`, etc).

Usually it looks like this

```
cloud.website.com
├── folder 1     <-- your own collective organisation
├── folder 2
└── website
    ├── doc
    └── www      <-- content of the website
        ├── articles
        ├── images
        .   
        └── pages
```
For more information look at the 3 markdown files in the `doc/` section