#!/usr/bin/bash

# get variable from .env file
if [ -f .env ]
then
  export $(cat .env | xargs)
fi

cd $THEME_FOLDER_PATH

# pulling from gitlab
git pull origin main
