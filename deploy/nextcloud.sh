#!/usr/bin/bash

# get variable from .env file
if [ -f .env ]
then
  export $(cat .env | xargs)
fi

# pulling from nextcloud
nextcloudcmd -u $NEXTCLOUD_USER -p $NEXTCLOUD_PASSWORD --path $REMOTE_FOLDER_PATH $LOCAL_FOLDER_PATH $NEXTCLOUD_ONLINE
