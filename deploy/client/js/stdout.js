
const socket = io();
console.log("socket.io loaded");


// ----- parse the outputs

// function parse_nextcloud(out){
//     let lines = out.split('\n');
//     let friendly = [];

//     friendly.push("[Changes]")
//     let changes = false;
//     for (let i = 0; i < lines.length; i++) {
//         let [line_header, line_data] = lines[i].split('\t');
//         // is an update info
//         if( line_header.includes("[ info nextcloud.sync.csync.updater ]")){
//             // is eval by nextcloud and is a file
//             if( line_data.includes("INSTRUCTION_EVAL") && line_data.includes(".") ){
//                 friendly.push(line_data.split(',')[0]);
//                 changes = true;
//             }
//         }
//     }
//     if(!changes){
//         friendly.push("no change")
//     }

//     return friendly.join('\n');
// }

function parse_stdout(name, stdout){
    // if(name == "nextcloud"){
    //     return parse_nextcloud(stdout);
    // } else{
        return stdout;
    // }
}


// ----- print to html stdout

function empty_stdout(name){
    console.log('emptying ' + name + ' stdout')

    let terminal_output = document.getElementById("stdout-"+name);
    let section = terminal_output.parentNode.closest('section');

    // default
    terminal_output.innerHTML = "";

    section.classList.add("running");
}

function write_stdout(name, error, stdout, stderr){
    console.log('writting ' + name + ' stdout')

    let terminal_output = document.getElementById("stdout-"+name);
    let section = terminal_output.parentNode.closest('section');

    let total_stdout = [stdout,error,stderr].join('<hr/>');
    total_stdout = parse_stdout(name, total_stdout);

    terminal_output.innerHTML = total_stdout;

    section.classList.remove("running");
    section.classList.add("done");
}


// ----- send command to execute

let bash_buttons = document.getElementsByClassName('bash-button');
for(let button of bash_buttons){
    button.addEventListener('click', function(){
        let exec = button.dataset.bash;
        let name = exec.split('-')[0];
        empty_stdout(name);
        socket.emit("exec " + exec, socket.id);
    });
}


// ----- we recieve the command line output

socket.on("stdout", (name, error, stdout, stderr) => {
    write_stdout(name, error, stdout, stderr);
});
