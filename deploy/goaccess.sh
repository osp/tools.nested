#!/usr/bin/bash

if [ -f .env ]
then
  export $(cat .env | xargs)
fi

# generating goaccess
goaccess /var/log/nginx/$MAIN_ONLINE/access.log -o $SERVE_FOLDER_PATH/goaccess.html --log-format=COMBINED
