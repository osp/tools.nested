const express = require('express');
const nunjucks = require('nunjucks');
const app = express();
const httpServer = require('http').Server(app);
const { Server } = require('socket.io');
const exec = require('child_process').exec;
require('dotenv').config()

// VARIABLES
// ------------------------------------------------------------------------

const hostname = "127.0.0.1";
const port = process.env.DEPLOY_PORT || 3000;

nunjucks.configure('views', {
    autoescape: true,
    express: app
});

// SERVER
// ------------------------------------------------------------------------

app.use(express.static('client'));

app.get('/', function(req, res) {
    res.render('index.html', { env: process.env });
});

const io = new Server(httpServer, {
    // we have to handle CORS as the client is not on the same domain
    // than those script (see client-svg.js)
    // https://socket.io/docs/v3/handling-cors/
    // https://github.com/expressjs/cors#configuration-options
    // cors: {
    //   origin: allowed_clients,
    //   methods: ["GET"]
    // }
});


// EXECUTE SERVER SIDE SCRIPTS
// ------------------------------------------------------------------------


function execute(command, callback){
    exec(command, function(error, stdout, stderr){ 
        callback(error, stdout, stderr); 
    });
};


// SOCKETS
// ------------------------------------------------------------------------

io.on("connection", (socket) => {

    console.log("new connection", socket.id);
    
    socket.on("exec nextcloud", () => {
        let command = "sh ./nextcloud.sh";
        let name = "nextcloud";
        console.log("2. exec:", command);
        execute(command, (error, stdout, stderr) => {
            io.emit("stdout", name, error, stdout, stderr)
        });
    });

    socket.on("exec gitlab", () => {
        let command = "sh ./gitlab.sh";
        let name = "gitlab";
        console.log("2. exec:", command);
        execute(command, (error, stdout, stderr) => {
            io.emit("stdout", name, error, stdout, stderr)
        });
    });

    socket.on("exec pelican-taste", () => {
        let command = "sh ./pelican-taste.sh";
        let name = "pelican";
        console.log("2. exec:", command);
        execute(command, (error, stdout, stderr) => {
            io.emit("stdout", name, error, stdout, stderr)
        });
    });
    socket.on("exec pelican-serve", () => {
        let command = "sh ./pelican-serve.sh";
        let name = "pelican";
        console.log("2. exec:", command);
        execute(command, (error, stdout, stderr) => {
            io.emit("stdout", name, error, stdout, stderr)
        });
    });

    socket.on("exec goaccess", () => {
        let command = "sh ./goaccess.sh";
        let name = "goaccess";
        console.log("2. exec:", command);
        execute(command, (error, stdout, stderr) => {
            io.emit("stdout", name, error, stdout, stderr)
        });
    });

});

// CREATE LISTEN
// ------------------------------------------------------------------------

httpServer.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);

    execute
});
