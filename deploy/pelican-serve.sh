#!/usr/bin/bash

if [ -f .env ]
then
  export $(cat .env | xargs)
fi

cd $THEME_FOLDER_PATH
#cd /home/debian/nest.tekhne/work.tekhne.www

# generating with pelican
/home/debian/bin/pelican -o $SERVE_FOLDER_PATH
