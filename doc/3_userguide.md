# Nested (user guide)

This document explain how to
1. how editing the content of the website works
2. how to deploy (update) the website after editing
3. how to setup your computer for better editing

**Nested** is the name of a system to manage and serve websites made by OSP. It is constructed around:

* **Nextcloud** as the shared-content hoster
* **Pelican** as the website generator

It focuses on managing a website in collaborative manner, in a way that is both accessible and as close as it can to raw files. It set a bridge between customized local edits in your own environment and online shared content, using tree-view files system as support architecture, and starting a permeability between internal organisation and public sharing.

## Overview

```mermaid
flowchart LR

    nextcloud[("nextcloud (content)")]
    git[("git (code)")]
    server[(server)]
    user[(user)]

    I1([cloud.website.com])
    I2([website.com])
    I3([deploy.website.com])
    I4([taste.website.com])
    
    nextcloud <--sync--> user

    nextcloud --sync--> server
    I3 --activate--> server
    git --pull--> server
    

    I1 --access--> nextcloud


    server --public--> I2
    server --preview--> I4

    classDef default stroke:#ccc, stroke-width: 1;
    classDef user stroke:hotpink, stroke-width: 4;
    classDef service stroke:SpringGreen, stroke-width: 4;
    classDef domain stroke:DeepSkyBlue, stroke-width: 4;

    class nextcloud,git,server service
    class user user
    class I1,I2,I3,I4 domain
```

**All the content of the website is on a specific `www` folder of your Nextcloud instance.** 

The content present itself as a collection of files nested inside subfolders, usually markdown `.md` file, alongside other media files (`jpg`, `mp3`, etc).

Usually it looks like this

```
cloud.website.com
├── folder 1
├── folder 2
└── website
    ├── doc
    └── www      <-- content of the website
        ├── articles
        ├── images
        .   
        └── pages
```

## Edit

The content of the website can be edited  in two ways:
1. By accessing the `www` folder through the **nextcloud web interface** (at cloud.website.com) and editing the files in its subfolders. It is super important to edit the markdown files by following those exact step
   1. Click on the **(...)** at the right of the file you want to edit
   2. Select **Edit with plain text editor**
2. By installing the **nextcloud sync client** and editing the file on your own computer, through the software of your choice. Think about this second way as if the software on your desktop & your personnal file system become the place to edit the website. More on that later.

Multiple people can edit the website both through the web-interface or through their own desktop with the sync client. However it is better to not edit the exact same file at the same time, as Nextcloud can have problem handling synchronisation of this content.

## Deploy

After modifying the content using either of those two ways, you have to **deploy** the website again: meaning to ask the server to regenerate the website. To do so:

1. Go to the deploy page (usually <https://deploy.website.com>) in your browser.
2. Clic on the first **Nextcloud sync** button, waiting for the server to answer. It should list the files that have been modified or added.
3. The second button **Gitlab pull** is for developper only, when we update the website.
4. Clic on the third **Pelican taste** button to preview the website (usually <https://taste.website.com>). This version is visible by anyone, but because you won't officially communicate the url, it is unlisted.
5. Once everybody confirmed their changes and it previewed well on the taste subdomain, clic on the **Pelican serve** button to publish it on the main domain.

## Create a post

Avoid spaces or special character in filenames, try to name them with a regular syntax like `2023-09-23_blog-post-1.md`

You can **create new articles** or posts by
1. Clicking on **(+)** at the top of the nextcloud web-interface, once in a folder
2. Select **New text file**
3. Name it such that it ends with the `.md` extension.

You need to write in Markdown, as it is the syntax we use for the content of the website.

> Markdown is a lightweight markup language, [...] designed to provide a syntax that is easy to read and write. A document tagged with Markdown can be read as is, without giving the impression of having been tagged or formatted by special instructions.

**Markdown** syntax is like a tool to create things like _headers of different levels_, _hyperlinks_, _bold_, _images_, etc. In a document that is made of plain text.
Here is a [short summary of important Markdown syntax](https://www.markdownguide.org/cheat-sheet/).

Every markdown file that constitute the content of the website should begin with certain **meta-data**, like so:

```YAML
title: "title of my post"
date: 2023-09-25
authors: first author, second author
tags: collaboration, open source
```

* only `title` is necessary
* `date` must have the `YYYY-MM-DD` formating and is used to sort posts by chronological order (most recent on top and such)
* `authors` and `tags` are list where items must be separated by comma

<!-- ## Allowing someone else to edit the content

You have two options

1. for **regular editors**, it's better to create an **Nextcloud** account for them, through which they will edit the markdown files.
2. for **punctual editors**, you can simply share a singular file by
   1. Click on the initial of the owner account (on the left of the **(...)**), it should open a sub-window on the right
   2. Click on the **(+)** at the right of **Share link**
   3. Click on the **(...)** that now appeared on the side of **Share link**
   4. Select **Allow editing**
   5. Paste the link to share it with someone else
   6. By accessing this link, they  -->

## Creating a Nextcloud account

For **regular editors**, it's better to create an **Nextcloud** account for them, through which they will edit the markdown files.

1. Connect to your account on the nextcloud instance (usually <https://cloud.website.com>)
2. Click on your profile picture at the top right
3. Select **Users**
4. Click on **Add user**
5. From there add the identifyer of the user, their email and a generic password that can be changed by temselves later on. Note that nextcloud identifyer can not be modified after their creation (but profile name can).

By default the new user won't be able to see anything (any folder of the cloud). Content has to be explicitly shared for them to access. There is two ways of shared content:

1. You can click on the initial next to a folder and then add specific user identifyer to the shared list.
2. You can create a new group, and add certain user to the group, then share a folder with a whole group using the previous method.

**Groups** can be useful, for example you can create a `website-editors` group, and share the `website/www/` folder with them. Meaning they can edit the Nextcloud content reserved as website content, but not the rest of the Nextcloud instance.

## Nextcloud sync client

To edit the website content directly through your own desktop softwares and file systems you need to first install the **nextcloud sync client**.

1. Install <https://nextcloud.com/features/#clients>
2. Create a folder on your computer that will contain the website content, and put it where it makes sense for you
3. Open the client, and create a new synchronisation
   1. Open `account`, then `add an acconpt` and login to the Nextcloud instance
   2. Click on your account and select `add new folder synchronisation`.
   3. Select the newly created folder on your computer, then the one on the Nextcloud instance that contain the content of the website (usually `website/www/`). 

Note that it is not necessary to synchronise and download the whole content of the Nextcloud locally, but only the folder that you want to edit. You can for example only synchronise a subfolder of `website/www/` if you only need to manage a part of content of the website and don't want to handle unnecessary gigabytes.

After the sync is set up, every modifications you'll do to the file in your cloud-synched folder will be automatically transfered to the Nextcloud instance, and then to the other users having a sync client. So if at least two people have the client, and the first one edit a file, the modification is going to propagate to the other users, normally in the minute it has been done, and reciprocally.

Starting from here you're free to edit any file that makes the content of the website with the software you prefer

* markdown text in your favorite markdown editor. it has be plain text though! i.e. do not edit with libre office softwares.
* images can be edited directly in GIMP or Photoshop
* audio in audacity, etc;

Note that after editing, it is important to ensure that the Nextcloud sync client logo shows a checkmark meaning it has finished propagating the changes online.

## Zettlr for easier editing

[Zettlr](https://www.zettlr.com/) is an open source software that allow easy editing of multiple file at once in Markdown.

It let you have the whole tree-view of the file system on the side, and edit different markdown files in multiple tabs, while correcting and hinting the syntax.

It has the nice functionnality to allow to **import images** by simply dragging them from a file system window to the Zettlr tab.

Note that you need to have the Nextcloud sync client installed and working for this step.
After installing Zettlr, you need to do some configuration.

1. Click on `file > open a new workspace`, and select your cloud-synched folder. This will open the whole tree view
2. Click on `file > preferences > preferences`,
   1. in the `general` tab select
      1. under `mode of handling files`, the option `combined`
      2. under `display file only`, the option `only filenames`
      3. the option `display markdown files extensions`
   2. in the `display` tab select
      1. `display images`

After that, Zettlr should open with this workspace everytime.