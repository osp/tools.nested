# Nested (VPS preparation)

This document explain how to
1. prepare a VPS to be used for this setup

## Take a VPS

1. take VPS at OVH (cost 5€/month on the lowest setting, with debian 11, you have to wait []min before being able to access to it). DOC: https://docs.ovh.com/fr/vps/debuter-avec-vps/.
2. setup the Automated Backup system, you will thank me later: I am actually rewritting this section after an incident...

start an ssh connection
1. by doing `ssh debian@IP` and entering the recieved password.
2. you can change user name and password
   
        sudo passwd username

## connect the VPS to a domain name

as the multiple website/nest can be hosted on this same vps it is recommanded to use a domain name of yours.

intro: https://docs.ovh.com/fr/domains/editer-ma-zone-dns/

for a namecheap domain:
1. In the namecheap admin panel, set up the type to BasicDNS (meaning no external NameServers, we write the DNS zone directly here in namecheap).
2. then we can edit this DNS zone and make an `A record`, with name `@` and the IP of the VPS. (https://support.us.ovhcloud.com/hc/en-us/articles/360012042099-How-to-Connect-Your-VPS-to-Your-Domain-Name#learn)
3. after that you'll be able to SSH using the domain name instead of the IP adress

        ssh user@domainname

## install Firewall on your VPS

Follow:
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-18-04

To understand first a bit how firewall works:
https://computer.howstuffworks.com/firewall.htm

Also "*Apparently there are quite a lot of bots testing random passwords combination so since recently I also install fail2ban*": 
https://www.howtogeek.com/675010/how-to-secure-your-linux-computer-with-fail2ban/

## install NGINX

`sudo apt install nginx`

Then open your domain name (or the IPV6 directly) in a browser, you should see a "Welcome to nginx!" page.

First step is to learn by reading a bit of documentation on how NGINX works.

NGINX configurations file:
https://docs.nginx.com/nginx/admin-guide/basic-functionality/managing-configuration-files/ 

Configuration for a webserver
https://docs.nginx.com/nginx/admin-guide/web-server/web-server/

Basic doc on how to serve static files
https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/


## install Pelican (both local and VPS)

1. check if you have python (on my debian I had `python3` installed)
2. install pip `sudo apt install python3-pip`
3. then use pip to install pelican `python3 -m pip install "pelican[markdown]"`

or (if error asking to make a venv)

1. `sudo apt install python3-pip`
2. `sudo apt install python3-venv`
3. `python3 -m venv ~`
4. `source bin/activate`
5. `pip3 install "pelican[markdown]"`
6. `pip3 list` should show pelican

`pelican --version` should work

## install git

because we already have a repo we can use it for this setup
1. `sudo apt install git`
2. as the VPS is an gitlab user in itself, it need an account and an ssh Key: https://docs.gitlab.com/ee/user/ssh.html

now is good time to clone your repository

## install Nextcloud-sync-client

1. `sudo apt install nextcloud-desktop` and `sudo apt install nextcloud-desktop-cmd` (for the command line client). Note: the command line client doesn't monitor changes, it just sync when activated (in both way). **Important:** if you already did a sync in the VPS folder, do not clean it by removing everything, because it can cause to delete everything from the cloud at the next call of the command-line sync.

## install nodejs for deploy page



## install certbot

<https://certbot.eff.org/instructions?ws=nginx&os=debianbuster>