
# Nested (Setup nests)

This document explain how to setup individual nest in your VPS by
1. setting up the nextcloud sync
2. setting up the gitlab sync
3. setting up the deploy page
4. serving the website
5. serving the taste (preview) version

## scheme

```mermaid
flowchart LR

    subgraph nextcloud
        C1[(content)]
    end

    I1 --access--> C1
    I1([cloud.example.com])

    subgraph git
        P1[[pelican]]
    end

    subgraph VPS
        C2[(content)]
        P2[[pelican]]
        O2[output]
        S((script))
        P2 -.symbolic link.-> C2
        P2 -.path to /var.-> O2
        S -- 1. activate synchroni --> C2
        S -- 2. activate pull --> P2
        S -- 3. activate generation --> O2
    end

    O2 --served--> I2
    O2 --draft--> I4
    I2([example.com])
    I4([taste.example.com])

    I3 --activate--> S
    I3([deploy.example.com])

    subgraph user_edit["user (editor)"]
        C4[(content)]
    end

    subgraph user_dev["user (dev)"]
        C3[(content)]
        P3[[pelican]]
        O3[output]
        P3 -.symbolic link.-> C3
        P3 -.subfolder.-> O3
    end

    C1 <--nextcloud client--> C4
    C1 <--nextcloud client---> C3
    C1 --nextcloud cmd client---> C2
    P1 <--pull/push--> P3
    P1 --pull--> P2

    classDef default stroke:#ccc, stroke-width: 1;
    classDef user stroke:hotpink, stroke-width: 4;
    classDef service stroke:SpringGreen, stroke-width: 4;
    classDef domain stroke:DeepSkyBlue, stroke-width: 4;
    class nextcloud,git,VPS service
    class user_edit,user_dev user
    class I1,I2,I3,I4 domain
```


## setting up a new nest

1. `ssh` to your VPS
2. `mkdir nest.example` and `cd` into it
3. `git clone` this repository inside, and make all the bash script executable with `sudo chmod 755`

### nextcloud setup

1. `mkdir cloud.example`
2. create or import `nextcloud.sh` script
3. create an `.env` file for the nest like that

        NAME="example"
        COLOR="hsl(142, 80%, 86%)"

        MAIN_ONLINE="example.com"
        NEXTCLOUD_ONLINE="cloud.example.com"
        GIT_ONLINE="gitlab.example.com"
        TASTE_ONLINE="taste.example.com"

        DEPLOY_PORT="3000"

        NEXTCLOUD_USER=" "
        NEXTCLOUD_PASSWORD=""

        LOCAL_FOLDER_PATH="/home/debian/nest.example/cloud.example/"
        REMOTE_FOLDER_PATH="/Website/www/"

        THEME_FOLDER_PATH='/home/debian/nest.example/work.example.www'
        SERVE_FOLDER_PATH='/var/www/example/'
        TASTE_FOLDER_PATH='/var/www/example.taste/'

4. launch the script `./nextcloud.sh` to sync the nest with the cloud, `cloud.example` should be filled with the content

### pelican setup

1. `git clone` your theme
2. `cd work.example.www`
3. create a symbolic link named content that redirect to the cloud part (this should of course replace all the local content, which should be hosted on the cloud).

        ln -s ../cloud.example/ content

    and add it to the `.gitignore`, don't forget to have `output` also in there.

### output folder

create output folder and change access rights so NGINX can read it.

        cd /var/www
        mkdir example
        sudo chown -R user:www-data example/
        sudo chmod 755 example/

### nginx setup

serve the output folder with nginx

1. `cd /etc/nginx/site-available`
2. `sudo nano example.com`
3. the file should look like this

        server {
            listen 80;
            server_name example.com;
            access_log  /var/log/nginx/access.log;
            error_log  /var/log/nginx/error.log;
            root /var/www/example/;
        }

4. we can add the symbolic link to `sites-enabled` and reload the NGINX server.

        cd ../sites-enabled
        sudo ln -s ../sites-available/example

5. then activate it by reloading NGINX service

        sudo service nginx configtest
        sudo service nginx reload


domain name should be set to point to the IP adress of the vps

## tasting

do the same last two step but with a `taste.` prefix for everything.

* `/var/www/example.taste`, with `chmod` and `chown`
* `/etc/nginx/sites-available/example.com.taste`
* serve to `taste.html`, a custom iframe template with a banner, instead of `index.html` per default
* create subdomain, or use wildcard

        sudo service nginx configtest
        sudo service nginx reload

## manual update while setting up

1. ssh to nestcloud VPS
2. `cd nest.example`
3. launch bash scripts manually, one after another to
   1. `nextcloud.sh`
   2. `gitlab.sh`
   3. `pelican-taste.sh`
   4. `pelican-serve.sh`

## certbot

1. https://certbot.eff.org/instructions?ws=nginx&os=debianstretch

## setting up deploy page

go to `nest/tools.nested/deploy` and `npm install`

settup a proxy nginx server like so

```                                   
server{

    # listen on port 80 on a subdomain
    listen 80;
    server_name deploy.cifas.be;

    # to log access and errors
    access_log  /var/log/nginx/access.log;
    error_log  /var/log/nginx/error.log;

    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;

        # pass every request from the subdomain to here
        proxy_pass http://127.0.0.1:3000;

        # needed for websocket
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

    }
}
```
and a systemctl file in `/etc/systemd/system/` name `example.service`

```
[Unit]
Description=page and script to deploy wifas website at deploy.example.be
Documentation=https://gitlab.constantvzw.org/osp/tools.nested
After=network.target

[Service]
Type=simple
User=debian
Group=debian
Environment=NODE_ENV=production
WorkingDirectory=/home/debian/nest.example/tools.nestcloud/deploy/
ExecStart=/home/debian/nest.example/tools.nestcloud/deploy/run.sh
Restart=always

[Install]
WantedBy=multi-user.target
``

Then start it and check if status is ok

    sudo systemctl start example.service
    sudo systemctl status example.service